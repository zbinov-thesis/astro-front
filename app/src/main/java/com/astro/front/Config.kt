package com.astro.front

class Config {
    companion object {
        const val BASE_URL = "http://10.0.2.2:8080"
//        const val BASE_URL = "http://192.168.1.108:8080"

        const val REQUIRE_FINE_GRANULARITY = false
        const val PASSIVE_MODE = false
        const val UPDATE_INTERVAL_IN_MILLISECONDS = 2000L
        const val REQUIRE_NEW_LOCATION = false

        const val REQUEST_VALUE_LOCATION = 1

        const val PREFERENCES_BUILDINGS_VISIBILITY = "buildingsVisibility"
        const val PREFERENCES_INSTALLATION_ID = "installationId"
        const val PREFERENCES_SPECIFICATIONS_HISTORY = "specificationHistory"

        const val EXTRA_EVENT_ID = "eventId"
        const val EXTRA_EVENT_TITLE = "eventTitle"
        const val EXTRA_OBSERVATION_ID = "observationId"

        var INSTALLATION_ID: String? = null
    }
}
