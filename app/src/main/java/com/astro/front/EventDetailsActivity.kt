package com.astro.front

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.astro.front.Config.Companion.BASE_URL
import com.astro.front.Config.Companion.EXTRA_EVENT_ID
import com.astro.front.Config.Companion.EXTRA_EVENT_TITLE
import com.astro.front.entities.Event
import com.astro.front.services.EventService
import com.astro.front.utilities.Utilities.Companion.formatDate
import com.astro.front.utilities.addTag
import com.cunoraz.tagview.TagView
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class EventDetailsActivity : AppCompatActivity() {

    private var progressBar: ProgressBar? = null

    private val gson = GsonBuilder().setLenient().create()
    private val eventService = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()
        .create(EventService::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setContentView(R.layout.activity_event_details)
        progressBar = findViewById(R.id.event_details_progress_bar)

        pullData()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.observationsMap -> {
                val intent = Intent(this, MapActivity::class.java)
                intent.putExtra(EXTRA_EVENT_ID, this.intent.getLongExtra(EXTRA_EVENT_ID, -1L))
                intent.putExtra(EXTRA_EVENT_TITLE, this.intent.getStringExtra(EXTRA_EVENT_TITLE))
                startActivity(intent)
                true
            }
            R.id.addObservation -> {
                val intent = Intent(this, ObservationAddActivity::class.java)
                intent.putExtra(EXTRA_EVENT_ID, this.intent.getLongExtra(EXTRA_EVENT_ID, -1L))
                intent.putExtra(EXTRA_EVENT_TITLE, this.intent.getStringExtra(EXTRA_EVENT_TITLE))
                startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.action_bar_with_observation_buttons, menu)
        return true
    }

    private fun pullData() {
        progressBar?.visibility = View.VISIBLE
        eventService.getById(this.intent.getLongExtra(EXTRA_EVENT_ID, -1L)).enqueue(object : Callback<Event> {
            override fun onResponse(call: Call<Event>, response: Response<Event>) {
                if (response.isSuccessful) {
                    progressBar?.visibility = View.GONE
                    setUpData(response.body())
                } else {
                    println(response.errorBody()?.string())
                    Toast.makeText(
                        this@EventDetailsActivity,
                        applicationContext.getString(R.string.server_response_error),
                        Toast.LENGTH_LONG
                    ).show()
                    progressBar?.visibility = View.GONE
                }
            }

            override fun onFailure(call: Call<Event>, t: Throwable) {
                t.printStackTrace()
                Toast.makeText(
                    this@EventDetailsActivity,
                    applicationContext.getString(R.string.connection_error),
                    Toast.LENGTH_LONG
                ).show()
                progressBar?.visibility = View.GONE
            }
        })
    }

    private fun setUpData(event: Event?) {
        if (event != null && event.isInitialized()) {
            findViewById<ScrollView>(R.id.event_details_scroll_view)?.visibility = View.VISIBLE
            supportActionBar?.title = event.title
            findViewById<TextView>(R.id.event_details_description)?.text = event.description
            findViewById<TextView>(R.id.event_details_start_date)?.text = formatDate(event.startDate)
            when (!event.endDate?.toString().isNullOrEmpty()) {
                true -> findViewById<TextView>(R.id.event_details_end_date)?.text = event.endDate?.let { formatDate(it) }
                else -> {
                    findViewById<TextView>(R.id.event_details_end_date)?.height = 0
                    findViewById<TextView>(R.id.event_details_end_date_label)?.apply {
                        text = null
                        height = 0
                    }
                    findViewById<TableRow>(R.id.event_details_end_date_row)?.setPadding(0, 0, 0, 0)
                }
            }
            val tagView = findViewById<TagView>(R.id.event_details_tags)
            event.tags?.forEach { tagView?.addTag(it.title) }
        } else {
            Toast.makeText(
                this,
                applicationContext.getString(R.string.something_went_wrong),
                Toast.LENGTH_LONG
            ).show()
            finish()
        }
    }
}
