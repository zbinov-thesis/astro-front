package com.astro.front

import android.R.color
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.astro.front.Config.Companion.EXTRA_EVENT_ID
import com.astro.front.Config.Companion.EXTRA_EVENT_TITLE
import com.astro.front.entities.dtos.EventDtoPullBasic
import com.astro.front.services.EventService
import com.astro.front.utilities.MainAdapter
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

class MainActivity : AppCompatActivity() {

    private var thisView: RecyclerView? = null
    private var retryButton: Button? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var progressBar: ProgressBar? = null
    private var sharedPreferences: SharedPreferences? = null

    private var events = listOf<EventDtoPullBasic>()

    private val gson = GsonBuilder().setLenient().create()
    private val eventService = Retrofit.Builder()
        .baseUrl(Config.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()
        .create(EventService::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        supportActionBar?.hide() // TODO: add drawer/settings menu
        setContentView(R.layout.activity_main)

        Config.INSTALLATION_ID = sharedPreferences?.getString(
            Config.PREFERENCES_INSTALLATION_ID, null
        )
        if (Config.INSTALLATION_ID == null) {
            Config.INSTALLATION_ID = UUID.randomUUID().toString()
            sharedPreferences?.edit()?.putString(Config.PREFERENCES_INSTALLATION_ID, Config.INSTALLATION_ID)?.apply()
        }

        thisView = findViewById(R.id.main_recycler)
        thisView?.layoutManager = LinearLayoutManager(this)
        thisView?.addOnItemTouchListener(
            OnItemTouchListener(applicationContext, object : ClickListener {
                override fun onClick(view: View, position: Int) {
                    val intent = Intent(this@MainActivity, EventDetailsActivity::class.java)
                    intent.putExtra(EXTRA_EVENT_ID, events[position].id)
                    intent.putExtra(EXTRA_EVENT_TITLE, events[position].title)
                    startActivity(intent)
                }
            })
        )

        retryButton = findViewById(R.id.main_button_retry)
        retryButton?.setOnClickListener {
            it?.visibility = View.GONE
            pullData()
        }

        swipeRefreshLayout = findViewById(R.id.main_swipe_container)
        swipeRefreshLayout?.setOnRefreshListener { pullData() }
        swipeRefreshLayout?.setColorSchemeResources(
            color.holo_red_light,
            color.holo_orange_light,
            color.holo_blue_dark
        );

        progressBar = findViewById(R.id.main_progress_bar)

        pullData()
    }

    private fun pullData() {
        if (events.isEmpty()) {
            progressBar?.visibility = View.VISIBLE
        }
        eventService.getAllBasic().enqueue(object : Callback<List<EventDtoPullBasic>> {
            override fun onResponse(call: Call<List<EventDtoPullBasic>>, response: Response<List<EventDtoPullBasic>>) {
                if (response.isSuccessful) {
                    progressBar?.visibility = View.GONE
                    response.body()?.let { list ->
                        events = list.filter { it.isInitialized() && it.id != null }
                        thisView?.adapter = MainAdapter(events)
                    }
                } else {
                    println(response.errorBody()?.string())
                    Toast.makeText(
                        this@MainActivity,
                        applicationContext.getString(R.string.server_response_error),
                        Toast.LENGTH_LONG
                    ).show()
                    progressBar?.visibility = View.GONE
                    if (events.isEmpty()) {
                        retryButton?.visibility = View.VISIBLE
                    }
                }
                swipeRefreshLayout?.isRefreshing = false
            }

            override fun onFailure(call: Call<List<EventDtoPullBasic>>, t: Throwable) {
                t.printStackTrace()
                Toast.makeText(
                    this@MainActivity,
                    applicationContext.getString(R.string.connection_error),
                    Toast.LENGTH_LONG
                ).show()
                progressBar?.visibility = View.GONE
                if (events.isEmpty()) {
                    retryButton?.visibility = View.VISIBLE
                }
                swipeRefreshLayout?.isRefreshing = false
            }
        })
    }

    private interface ClickListener {
        fun onClick(view: View, position: Int)
    }

    private class OnItemTouchListener(context: Context, private val clickListener: ClickListener) : RecyclerView.SimpleOnItemTouchListener() {

        private val gestureDetector = GestureDetector(context, object : GestureDetector.SimpleOnGestureListener() {
            override fun onSingleTapUp(e: MotionEvent): Boolean = true
        })

        override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
            val child = rv.findChildViewUnder(e.x, e.y) ?: return false
            if (gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildAdapterPosition(child))
            }
            return false
        }
    }
}
