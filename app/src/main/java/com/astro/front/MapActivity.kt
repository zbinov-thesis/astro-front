package com.astro.front

import android.Manifest.permission
import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.os.PersistableBundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.preference.PreferenceManager
import com.astro.front.Config.Companion.EXTRA_EVENT_ID
import com.astro.front.Config.Companion.EXTRA_EVENT_TITLE
import com.astro.front.Config.Companion.EXTRA_OBSERVATION_ID
import com.astro.front.entities.enums.Rating
import com.astro.front.services.ObservationService
import com.astro.front.utilities.Utilities.Companion.askForLocationPermission
import com.astro.front.utilities.Utilities.Companion.isPermissionGranted
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.GsonBuilder
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.annotations.BubbleLayout
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.LocationComponentOptions
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.MapboxMap.OnMapClickListener
import com.mapbox.mapboxsdk.maps.MapboxMap.OnMapLongClickListener
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.building.BuildingPlugin
import com.mapbox.mapboxsdk.style.expressions.Expression.*
import com.mapbox.mapboxsdk.style.layers.HillshadeLayer
import com.mapbox.mapboxsdk.style.layers.Property
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.*
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import com.mapbox.mapboxsdk.style.sources.RasterDemSource
import com.mapbox.mapboxsdk.utils.BitmapUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.ref.WeakReference

class MapActivity : AppCompatActivity(), OnMapReadyCallback, OnMapClickListener, OnMapLongClickListener {

    private var thisView: MapView? = null
    private lateinit var mapboxMap: MapboxMap
    private var sharedPreferences: SharedPreferences? = null

    private var source: HashMap<String, GeoJsonSource?> = HashMap()
    private var featureCollection: FeatureCollection? = null

    private val gson = GsonBuilder().setLenient().create()
    private val observationService = Retrofit.Builder()
        .baseUrl(Config.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()
        .create(ObservationService::class.java)

    companion object {
        private const val GEOJSON_SOURCE_ID = "geojson-source"
        private const val MARKER_IMAGE_ID = "marker-image"
        private const val MARKER_LAYER_ID = "marker-layer"
        private const val CALLOUT_LAYER_ID = "callout-layer"
        private const val PROPERTY_ID = "id"
        private const val PROPERTY_SELECTED = "selected"
        private const val PROPERTY_TIMESTAMP = "timestamp"
        private const val PROPERTY_RATING = "rating"
        private const val PROPERTY_MESSAGE = "message"
    }

    //<editor-fold desc="AppCompatActivity">
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = this.intent.getStringExtra(EXTRA_EVENT_TITLE)
        setContentView(R.layout.activity_map)

        askForLocationPermission(this@MapActivity, this@MapActivity)
        thisView = findViewById(R.id.map_mapView)
        thisView?.onCreate(savedInstanceState)
        thisView?.getMapAsync(this)
    }

    override fun onStart() {
        super.onStart()
        thisView?.onStart()
    }

    override fun onStop() {
        thisView?.onStop()
        super.onStop()
    }

    override fun onPause() {
        thisView?.onPause()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        thisView?.onResume()
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        outState?.let { thisView?.onSaveInstanceState(it) }
        super.onSaveInstanceState(outState, outPersistentState)
    }

    override fun onLowMemory() {
        thisView?.onLowMemory()
        super.onLowMemory()
    }

    override fun onDestroy() {
        if (::mapboxMap.isInitialized) {
            mapboxMap.removeOnMapLongClickListener(this)
            mapboxMap.removeOnMapClickListener(this)
        }
        thisView?.onDestroy()
        super.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.addObservation -> {
                val intent = Intent(this, ObservationAddActivity::class.java)
                intent.putExtra(EXTRA_EVENT_ID, this.intent.getLongExtra(EXTRA_EVENT_ID, -1L))
                intent.putExtra(EXTRA_EVENT_TITLE, this.intent.getStringExtra(EXTRA_EVENT_TITLE))
                startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.action_bar_for_map_view, menu)
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            Config.REQUEST_VALUE_LOCATION -> {
                if (::mapboxMap.isInitialized && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mapboxMap.style?.let { enableLocationComponentIfPermissionGranted(it) }
                }
            }
        }
    }
    //</editor-fold>

    //<editor-fold desc="OnMapReadyCallback">
    override fun onMapReady(mapboxMap: MapboxMap) {
        this.mapboxMap = mapboxMap
        pullData()

        mapboxMap.setStyle(Style.MAPBOX_STREETS) { style ->
            removeUnnecessaryLayers(style)
            enableLocationComponentIfPermissionGranted(style)
            enableBuildingsAndSetActionButtonListener(style)
            addHillshadeLayer(style) // TODO: uncomment
            mapboxMap.addOnMapClickListener(this@MapActivity)
            mapboxMap.addOnMapLongClickListener(this@MapActivity)
        }
    }

    private fun removeUnnecessaryLayers(style: Style) {
        style.removeLayer("road-number-shield")
        style.removeLayer("golf-hole-line")
        style.removeLayer("golf-hole-label")
        style.removeLayer("tunnel-oneway-arrow-blue")
        style.removeLayer("tunnel-oneway-arrow-white")
        style.removeLayer("road-oneway-arrow-blue")
        style.removeLayer("road-oneway-arrow-white")
        style.removeLayer("bridge-oneway-arrow-blue")
        style.removeLayer("bridge-oneway-arrow-white")
    }

    private fun enableLocationComponentIfPermissionGranted(style: Style) {
        if (isPermissionGranted(this, permission.ACCESS_FINE_LOCATION)) {
            val locationComponentOptions = LocationComponentOptions.builder(this)
                .trackingGesturesManagement(true)
                .accuracyColor(ContextCompat.getColor(this, R.color.mapbox_blue))
                .build()
            val locationComponentActivationOptions = LocationComponentActivationOptions.builder(this, style)
                .locationComponentOptions(locationComponentOptions)
                .build()
            mapboxMap.locationComponent.apply {
                activateLocationComponent(locationComponentActivationOptions)
                isLocationComponentEnabled = true
                renderMode = RenderMode.COMPASS
                cameraMode = CameraMode.TRACKING
                if (lastKnownLocation == null) {
                    mapboxMap.cameraPosition = CameraPosition.DEFAULT
                }
            }
        } else {
            mapboxMap.cameraPosition = CameraPosition.DEFAULT
        }
    }

    private fun enableBuildingsAndSetActionButtonListener(style: Style) {
        val buildingPlugin = thisView?.let { BuildingPlugin(it, mapboxMap, style) }?.apply {
            setVisibility(sharedPreferences?.getBoolean(Config.PREFERENCES_BUILDINGS_VISIBILITY, true) ?: true)
            setOpacity(0.33f)
            setMinZoomLevel(4f)
        }
        findViewById<FloatingActionButton>(R.id.map_buildings_switch).setOnClickListener {
            buildingPlugin?.run {
                setVisibility(!isVisible)
                sharedPreferences?.edit()?.putBoolean(Config.PREFERENCES_BUILDINGS_VISIBILITY, isVisible)?.apply()
            }
        }
    }

    private fun addHillshadeLayer(style: Style) {
        style.addSource(
            RasterDemSource("hillshade-source", "mapbox://mapbox.terrain-rgb")
        )
        val hillshadeLayer = HillshadeLayer("hillshade-layer", "hillshade-source")
            .withProperties(
                hillshadeHighlightColor(Color.parseColor("#00BD32")),
                hillshadeShadowColor(Color.BLACK)
            )
        style.addLayerBelow(hillshadeLayer, "aerialway")
    }
    //</editor-fold>

    //<editor-fold desc="Symbols">
    override fun onMapClick(point: LatLng): Boolean {
        val features = getFeaturesFromMarkerLayers(point)
        if (features.isEmpty()) {
            return false
        }

        val id = features[0].getStringProperty(PROPERTY_ID)
        featureCollection?.features()?.forEach { feature ->
            if (feature.getStringProperty(PROPERTY_ID) == id) {
                if (feature.getBooleanProperty(PROPERTY_SELECTED)) {
                    setFeatureSelectState(feature, false)
                } else {
                    featureCollection?.features()?.forEach {
                        if (it.getBooleanProperty(PROPERTY_SELECTED)) {
                            setFeatureSelectState(it, false)
                        }
                    }
                    setFeatureSelectState(feature, true)
//                    refreshSource() // TODO: check if needed
                }
            }
        }

        return true
    }

    override fun onMapLongClick(point: LatLng): Boolean {
        val features = getFeaturesFromMarkerLayers(point)
        if (features.isEmpty()) {
            return false
        }

        val id = features[0].getStringProperty(PROPERTY_ID)
        featureCollection?.features()?.forEach { feature ->
            if (feature.getStringProperty(PROPERTY_ID) == id) {
                val intent = Intent(this, ObservationDetailsActivity::class.java)
                intent.putExtra(EXTRA_OBSERVATION_ID, id.toLong())
                startActivity(intent)
            }
        }

        return true
    }

    private fun setFeatureSelectState(feature: Feature, selectedState: Boolean) {
        feature.properties()?.run {
            addProperty(PROPERTY_SELECTED, selectedState)
            refreshSource()
        }
    }

    private fun getFeaturesFromMarkerLayers(point: LatLng): List<Feature> {
        return mapboxMap.queryRenderedFeatures(
            mapboxMap.projection.toScreenLocation(point),
            MARKER_LAYER_ID + Rating.FULLY_OBSERVABLE.toString(),
            MARKER_LAYER_ID + Rating.HARDLY_OBSERVABLE.toString(),
            MARKER_LAYER_ID + Rating.UNOBSERVABLE.toString()
        )
    }

    private fun setUpData(featureCollection: FeatureCollection) {
        this.featureCollection = featureCollection

        val featureListsByRating = hashMapOf<String, MutableList<Feature>>(
            Rating.FULLY_OBSERVABLE.toString() to mutableListOf(),
            Rating.HARDLY_OBSERVABLE.toString() to mutableListOf(),
            Rating.UNOBSERVABLE.toString() to mutableListOf()
        )
        featureCollection.features()?.forEach {
            featureListsByRating[it.getStringProperty(PROPERTY_RATING)]?.add(it)
        }

        mapboxMap.getStyle { style ->
            addBitmaps(style)
            featureListsByRating.forEach { entry ->
                source[GEOJSON_SOURCE_ID + entry.key] = GeoJsonSource(GEOJSON_SOURCE_ID + entry.key, FeatureCollection.fromFeatures(entry.value))
                source[GEOJSON_SOURCE_ID + entry.key]?.let { style.addSource(it) }

                style.addLayer(
                    SymbolLayer(MARKER_LAYER_ID + entry.key, GEOJSON_SOURCE_ID + entry.key)
                        .withProperties(
                            iconImage(MARKER_IMAGE_ID + entry.key),
                            iconOffset(arrayOf(0f, -8f)),
                            iconAllowOverlap(true)
                        )
                )
            }
            addCalloutLayer(style)
        }
    }

    private fun addBitmaps(style: Style) {
        val drawable = ResourcesCompat.getDrawable(resources, R.drawable.map_marker, null)
        val bitmaps = hashMapOf(
            MARKER_IMAGE_ID + Rating.FULLY_OBSERVABLE.toString() to BitmapUtils.getBitmapFromDrawable(drawable?.also { DrawableCompat.setTint(it, Color.BLACK) }),
            MARKER_IMAGE_ID + Rating.HARDLY_OBSERVABLE.toString() to BitmapUtils.getBitmapFromDrawable(drawable?.also { DrawableCompat.setTint(it, Color.DKGRAY) }),
            MARKER_IMAGE_ID + Rating.UNOBSERVABLE.toString() to BitmapUtils.getBitmapFromDrawable(drawable?.also { DrawableCompat.setTint(it, Color.GRAY) })
        )
        style.addImages(bitmaps)
    }

    private fun addCalloutLayer(style: Style) {
        source[GEOJSON_SOURCE_ID] = GeoJsonSource(GEOJSON_SOURCE_ID, featureCollection)
        source[GEOJSON_SOURCE_ID]?.let { style.addSource(it) }
        style.addLayer(
            SymbolLayer(CALLOUT_LAYER_ID, GEOJSON_SOURCE_ID)
                .withProperties(
                    iconImage("{id}"),
                    iconAnchor(Property.ICON_ANCHOR_BOTTOM),
                    iconOffset(arrayOf(0f, -25f))
                )
                .withFilter(
                    eq(get(PROPERTY_SELECTED), literal(true))
                )
        )
    }

    private fun refreshSource() {
        featureCollection?.let {
            source[GEOJSON_SOURCE_ID]?.setGeoJson(it)
        }
    }

    private class GenerateInfoWindowsAsyncTask(activity: MapActivity, private val refreshSource: Boolean) : AsyncTask<FeatureCollection, Unit, HashMap<String, Bitmap>>() {

        private val viewMap = HashMap<String, View>() // TODO: seems to be unnecessary - delete?
        private val activityReference: WeakReference<MapActivity> = WeakReference(activity)

        constructor(activity: MapActivity) : this(activity, false)

        override fun doInBackground(vararg params: FeatureCollection?): HashMap<String, Bitmap>? {
            val activity = activityReference.get() ?: return null
            val bitmaps = HashMap<String, Bitmap>()

            val featureCollection = params[0]
            featureCollection?.features()?.forEach {
                @Suppress("DEPRECATION")
                @SuppressLint("InflateParams")
                val bubbleLayout = LayoutInflater.from(activity).inflate(
                    R.layout.info_popup, null
                ) as BubbleLayout

                val id = it.getStringProperty(PROPERTY_ID)

                bubbleLayout.findViewById<TextView>(R.id.info_popup_top_label).text = it.getStringProperty(PROPERTY_TIMESTAMP)
                bubbleLayout.findViewById<TextView>(R.id.info_popup_rating).text = it.getStringProperty(PROPERTY_RATING)
                with (it.getStringProperty(PROPERTY_MESSAGE)) {
                    bubbleLayout.findViewById<TextView>(R.id.info_popup_message).text = when (this.length < 30) {
                        true -> this
                        else -> this.substring(0, 30) + "…"
                    }
                }

                val measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                bubbleLayout.measure(measureSpec, measureSpec)
                bubbleLayout.arrowPosition = bubbleLayout.measuredWidth / 2f - 15f

                bitmaps[id] = generateBitmap(bubbleLayout)
                viewMap[id] = bubbleLayout
            }

            return bitmaps
        }

        override fun onPostExecute(bitmapHashMap: HashMap<String, Bitmap>?) {
            super.onPostExecute(bitmapHashMap)
            val activity = activityReference.get() ?: return

            bitmapHashMap?.let {
                activity.mapboxMap.getStyle { style ->
                    style.addImages(it)
                }
                if (refreshSource) {
                    activity.refreshSource()
                }
            }
        }

        private fun generateBitmap(view: View): Bitmap {
            val measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
            view.apply {
                measure(measureSpec, measureSpec)
                layout(0, 0, measuredWidth, measuredHeight)
            }
            return Bitmap.createBitmap(view.measuredWidth, view.measuredHeight, Bitmap.Config.ARGB_8888)
                .apply {
                    eraseColor(Color.TRANSPARENT)
                    view.draw(
                        Canvas(this)
                    )
                }
        }
    }
    //</editor-fold>

    private fun pullData() {
        observationService.getFeatureCollection(this.intent.getLongExtra(EXTRA_EVENT_ID, -1L)).enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                if (response.isSuccessful) {
                    response.body()?.let { responseBody ->
                        with(FeatureCollection.fromJson(gson.toJson(responseBody))) {
                            setUpData(this)
                            GenerateInfoWindowsAsyncTask(this@MapActivity).execute(this)
                        }
                    }
                } else {
                    println(response.errorBody()?.string())
                    Toast.makeText(
                        this@MapActivity,
                        applicationContext.getString(R.string.server_response_error),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                Toast.makeText(
                    this@MapActivity,
                    applicationContext.getString(R.string.connection_error),
                    Toast.LENGTH_LONG
                ).show()
                t.printStackTrace()
            }
        })
    }
}
