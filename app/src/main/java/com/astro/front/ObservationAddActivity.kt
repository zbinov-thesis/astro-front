package com.astro.front

import android.Manifest.permission
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import com.astro.front.Config.Companion.EXTRA_EVENT_ID
import com.astro.front.Config.Companion.EXTRA_EVENT_TITLE
import com.astro.front.entities.Event
import com.astro.front.entities.Observation
import com.astro.front.entities.Position
import com.astro.front.entities.dtos.ObservationDtoPost
import com.astro.front.entities.enums.Rating
import com.astro.front.services.ObservationService
import com.astro.front.utilities.Utilities.Companion.askForLocationPermission
import com.astro.front.utilities.Utilities.Companion.isPermissionGranted
import com.google.gson.GsonBuilder
import im.delight.android.location.SimpleLocation
import okhttp3.internal.toImmutableList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.sql.Timestamp
import java.util.*

class ObservationAddActivity : AppCompatActivity() {

    private var location: SimpleLocation? = null
    private var progressBar: ProgressBar? = null
    private var sharedPreferences: SharedPreferences? = null

    private var specificationHistory = ArrayDeque<String>()

    private val gson = GsonBuilder().setLenient().create()
    private val observationService = Retrofit.Builder()
        .baseUrl(Config.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()
        .create(ObservationService::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setContentView(R.layout.activity_observation_add)

        location = SimpleLocation(
            this,
            Config.REQUIRE_FINE_GRANULARITY,
            Config.PASSIVE_MODE,
            Config.UPDATE_INTERVAL_IN_MILLISECONDS,
            Config.REQUIRE_NEW_LOCATION
        )
        askForLocationPermission(this, this)
//        location?.hasLocationEnabled()?.let {
//            if (!it && Config.REQUIRE_FINE_GRANULARITY) {
//                SimpleLocation.openSettings(this)
//            }
//        }

        progressBar = findViewById(R.id.observation_add_progress_bar)

        gson.fromJson(
            sharedPreferences?.getString(Config.PREFERENCES_SPECIFICATIONS_HISTORY, ""),
            specificationHistory::class.java
        )?.let {
            specificationHistory = it
        }

        val observation = Observation()
        setUpData(observation)
        addButtonListener(observation)
    }

    override fun onPause() {
        location?.endUpdates()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        if (isPermissionGranted(this, permission.ACCESS_FINE_LOCATION)) {
            location?.beginUpdates()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            Config.REQUEST_VALUE_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    location?.beginUpdates()
                } else {
                    location?.endUpdates()
                    Toast.makeText(
                        this,
                        R.string.location_permission,
                        Toast.LENGTH_LONG
                    ).show()
                    finish()
                }
            }
        }
    }

    private fun addButtonListener(observation: Observation) {
        findViewById<Button>(R.id.observation_add_post_button)?.setOnClickListener {
            Config.INSTALLATION_ID?.let { observation.userId = it }
            observation.timestamp = Timestamp(System.currentTimeMillis())
            observation.event = Event().also { it.id = this.intent.getLongExtra(EXTRA_EVENT_ID, -1L) }
            location?.let { observation.position = Position(it) }
            observation.specification = findViewById<AutoCompleteTextView>(R.id.observation_add_specification).text.toString()
            observation.message = findViewById<TextView>(R.id.observation_add_message).text.toString()

            if (observation.isInitialized()) {
                postData(observation)
            } else {
                if (findViewById<Spinner>(R.id.observation_add_rating_spinner).selectedItemPosition == 0) {
                    Toast.makeText(
                        this,
                        applicationContext.getString(R.string.rating_empty),
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    Toast.makeText(
                        this,
                        applicationContext.getString(R.string.something_went_wrong),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            if (specificationHistory.contains(observation.specification)) {
                specificationHistory.removeAll { it == observation.specification }
            }
            if (!observation.specification.isNullOrBlank()) {
                specificationHistory.addFirst(observation.specification)
            }
            if (specificationHistory.size > 5) {
                specificationHistory.removeLast()
            }

            gson.toJson(specificationHistory)?.let {
                sharedPreferences?.edit()?.putString(Config.PREFERENCES_SPECIFICATIONS_HISTORY, it)?.apply()
            }
        }
    }

    private fun postData(observation: Observation) {
        startProgressBarAndDisableTouch(progressBar, window)
        observationService.add(ObservationDtoPost(observation)).enqueue(object : Callback<Unit> {
            override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                if (response.isSuccessful) {
                    Toast.makeText(
                        this@ObservationAddActivity,
                        applicationContext.getString(R.string.post_successful),
                        Toast.LENGTH_LONG
                    ).show()
                    stopProgressBarAndEnableTouch(progressBar, window)
                    finish()
                } else {
                    println(response.errorBody()?.string())
                    Toast.makeText(
                        this@ObservationAddActivity,
                        applicationContext.getString(R.string.server_response_error),
                        Toast.LENGTH_LONG
                    ).show()
                    stopProgressBarAndEnableTouch(progressBar, window)
                }
            }

            override fun onFailure(call: Call<Unit>, t: Throwable) {
                t.printStackTrace()
                Toast.makeText(
                    this@ObservationAddActivity,
                    applicationContext.getString(R.string.connection_error),
                    Toast.LENGTH_LONG
                ).show()
                stopProgressBarAndEnableTouch(progressBar, window)
            }
        })
    }

    private fun startProgressBarAndDisableTouch(progressBar: ProgressBar?, window: Window) {
        progressBar?.visibility = View.VISIBLE
        window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
    }

    private fun stopProgressBarAndEnableTouch(progressBar: ProgressBar?, window: Window) {
        progressBar?.visibility = View.GONE
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    private fun setUpData(observation: Observation) {
        supportActionBar?.title = "${applicationContext.getString(R.string.add_observation)} ${this.intent.getStringExtra(EXTRA_EVENT_TITLE)}"

        val spinnerItems = mutableListOf<String>(applicationContext.getString(R.string.specify_rating)).apply {
            addAll(Rating.values().map { it.toString() })
        }.toImmutableList()

        findViewById<Spinner>(R.id.observation_add_rating_spinner)?.apply {
            adapter = object : ArrayAdapter<String>(
                this@ObservationAddActivity,
                android.R.layout.simple_spinner_dropdown_item,
                spinnerItems
            ) {
                override fun isEnabled(position: Int): Boolean {
                    return when (position) {
                        0 -> false
                        else -> true
                    }
                }

                override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
                    val view = super.getDropDownView(position, convertView, parent)
                    val textView = view as TextView

                    if (position == 0) {
                        textView.setTextColor(Color.GRAY)
                    }

                    return view
                }
            }

            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    when (parent?.getItemAtPosition(position) as String) {
                        Rating.FULLY_OBSERVABLE.toString() -> observation.rating = Rating.FULLY_OBSERVABLE
                        Rating.HARDLY_OBSERVABLE.toString() -> observation.rating = Rating.HARDLY_OBSERVABLE
                        Rating.UNOBSERVABLE.toString() -> observation.rating = Rating.UNOBSERVABLE
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
//                    TODO("not implemented")
                }
            }
        }

        findViewById<AutoCompleteTextView>(R.id.observation_add_specification)?.apply {
            setAdapter(
                ArrayAdapter<String>(
                    this@ObservationAddActivity,
                    android.R.layout.simple_dropdown_item_1line,
                    specificationHistory.toMutableList()
                )
            )

            setOnFocusChangeListener { _, _ -> showDropDown() }
            setOnClickListener { showDropDown() }
        }
    }
}
