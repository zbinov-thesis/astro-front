package com.astro.front

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.ScrollView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.astro.front.Config.Companion.EXTRA_OBSERVATION_ID
import com.astro.front.entities.Observation
import com.astro.front.services.ObservationService
import com.astro.front.utilities.Utilities.Companion.formatDate
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ObservationDetailsActivity : AppCompatActivity() {

    private var progressBar: ProgressBar? = null

    private val gson = GsonBuilder().setLenient().create()
    private val observationService = Retrofit.Builder()
        .baseUrl(Config.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()
        .create(ObservationService::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setContentView(R.layout.activity_observation_details)
        progressBar = findViewById(R.id.observation_details_progress_bar)

        pullData()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun pullData() {
        progressBar?.visibility = View.VISIBLE
        observationService.getById(this.intent.getLongExtra(EXTRA_OBSERVATION_ID, -1L)).enqueue(object : Callback<Observation> {
            override fun onResponse(call: Call<Observation>, response: Response<Observation>) {
                if (response.isSuccessful) {
                    progressBar?.visibility = View.GONE
                    setUpData(response.body())
                } else {
                    println(response.errorBody()?.string())
                    Toast.makeText(
                        this@ObservationDetailsActivity,
                        applicationContext.getString(R.string.server_response_error),
                        Toast.LENGTH_LONG
                    ).show()
                    progressBar?.visibility = View.GONE
                }
            }

            override fun onFailure(call: Call<Observation>, t: Throwable) {
                t.printStackTrace()
                Toast.makeText(
                    this@ObservationDetailsActivity,
                    applicationContext.getString(R.string.connection_error),
                    Toast.LENGTH_LONG
                ).show()
                progressBar?.visibility = View.GONE
            }
        })
    }

    private fun setUpData(observation: Observation?) {
        if (observation != null && observation.isInitialized()) {
            findViewById<ScrollView>(R.id.observation_detail_scroll_view)?.visibility = View.VISIBLE
            supportActionBar?.title = "${applicationContext.getString(R.string.observation_of)} ${observation.event.title}"
            findViewById<TextView>(R.id.observation_details_date)?.text = formatDate(observation.timestamp)
            findViewById<TextView>(R.id.observation_details_rating)?.text = observation.rating.toString()
            findViewById<TextView>(R.id.observation_details_specification)?.text = observation.specification
            findViewById<TextView>(R.id.observation_details_message)?.text = observation.message
        } else {
            Toast.makeText(
                this,
                applicationContext.getString(R.string.something_went_wrong),
                Toast.LENGTH_LONG
            ).show()
            finish()
        }
    }
}
