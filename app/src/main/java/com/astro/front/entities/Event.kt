package com.astro.front.entities

import java.sql.Timestamp

class Event {

    var id: Long? = null
    lateinit var startDate: Timestamp
    var endDate: Timestamp? = null
    lateinit var title: String
    lateinit var description: String
    var tags: Set<Tag>? = null

    fun isInitialized(): Boolean {
        return ::startDate.isInitialized
                && ::title.isInitialized
                && ::description.isInitialized
    }
}
