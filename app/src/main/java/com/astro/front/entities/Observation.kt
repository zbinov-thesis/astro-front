package com.astro.front.entities

import com.astro.front.entities.enums.Rating
import java.sql.Timestamp

class Observation {

    var id: Long? = null
    lateinit var userId: String
    lateinit var timestamp: Timestamp
    lateinit var event: Event
    lateinit var position: Position
    lateinit var rating: Rating
    var specification: String? = null
    var message: String? = null

    fun isInitialized(): Boolean {
        return ::userId.isInitialized
                && ::timestamp.isInitialized
                && ::event.isInitialized
                && ::position.isInitialized
                && ::rating.isInitialized
    }
}
