package com.astro.front.entities

import im.delight.android.location.SimpleLocation

class Position(location: SimpleLocation) {

    var id: Long? = null
    var longitude: Double = 0.0
    var latitude: Double = 0.0
    var altitude: Double = 0.0

    init {
        this.longitude = location.longitude
        this.latitude = location.latitude
        this.altitude = location.altitude
    }
}
