package com.astro.front.entities

class Tag {

    var id: Long? = null
    lateinit var title: String
    var description: String? = null

    fun isInitialized(): Boolean {
        return ::title.isInitialized
    }
}
