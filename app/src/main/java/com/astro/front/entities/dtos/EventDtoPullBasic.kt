package com.astro.front.entities.dtos

class EventDtoPullBasic {

    var id: Long? = null
    lateinit var title: String
    lateinit var description: String

    fun isInitialized(): Boolean {
        return ::title.isInitialized
                && ::description.isInitialized
    }
}
