package com.astro.front.entities.dtos

import android.annotation.SuppressLint
import com.astro.front.entities.Event
import com.astro.front.entities.Observation
import com.astro.front.entities.Position
import com.astro.front.entities.enums.Rating
import java.text.SimpleDateFormat

class ObservationDtoPost @SuppressLint("SimpleDateFormat") constructor(observation: Observation) {

    var id: Long? = null
    var userId: String
    var timestamp: String
    var event: Event
    var position: Position
    var rating: Rating
    var specification: String
    var message: String

    init {
        this.id = observation.id
        this.userId = observation.userId
        this.timestamp = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(observation.timestamp)
        this.event = observation.event
        this.position = observation.position
        this.rating = observation.rating
        this.specification = observation.specification ?: ""
        this.message = observation.message ?: ""
    }
}
