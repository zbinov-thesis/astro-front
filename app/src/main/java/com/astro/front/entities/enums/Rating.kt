package com.astro.front.entities.enums

enum class Rating {

    FULLY_OBSERVABLE,
    HARDLY_OBSERVABLE,
    UNOBSERVABLE;

    override fun toString(): String {
        return when (this) {
            FULLY_OBSERVABLE -> "Fully observable"
            HARDLY_OBSERVABLE -> "Hardly observable"
            UNOBSERVABLE -> "Unobservable"
        }
    }
}
