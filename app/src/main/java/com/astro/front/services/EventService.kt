package com.astro.front.services

import com.astro.front.entities.Event
import com.astro.front.entities.dtos.EventDtoPullBasic
import retrofit2.Call
import retrofit2.http.*

interface EventService {

    @GET("/event")
    fun getAll(): Call<List<Event>>

    @GET("/event/basic")
    fun getAllBasic(): Call<List<EventDtoPullBasic>>

    @GET("/event/{id}")
    fun getById(@Path("id") id: Long): Call<Event>

    @POST("/event")
    fun add(@Body event: Event): Call<Event>

    @PUT("/event/{id}")
    fun update(@Body event: Event, @Path("id") id: Long): Call<Event>

    @DELETE("/event/{id}")
    fun delete(@Path("id") id: Long)

    @GET("/event/filter")
    fun filter(@Query("search") query: String): Call<List<Event>>
}
