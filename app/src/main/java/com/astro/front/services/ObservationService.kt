package com.astro.front.services

import com.astro.front.entities.Observation
import com.astro.front.entities.dtos.ObservationDtoPost
import retrofit2.Call
import retrofit2.http.*

interface ObservationService {

    @GET("/observation")
    fun getAll(): Call<List<Observation>>

    @GET("/observation/featureCollection/{eventId}")
    fun getFeatureCollection(@Path("eventId") eventId: Long): Call<Any>

    @GET("/observation/{id}")
    fun getById(@Path("id") id: Long): Call<Observation>

    @POST("/observation")
    fun add(@Body observation: Observation): Call<Observation>

    @POST("/observation")
    fun add(@Body observation: ObservationDtoPost): Call<Unit>

    @PUT("/observation/{id}")
    fun update(@Body observation: Observation, @Path("id") id: Long): Call<Observation>

    @DELETE("/observation/{id}")
    fun delete(@Path("id") id: Long)

    @GET("/observation/filter")
    fun filter(@Query("search") query: String): Call<List<Observation>>
}
