package com.astro.front.utilities

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.astro.front.R
import com.astro.front.entities.dtos.EventDtoPullBasic

class MainAdapter(
    private var events: List<EventDtoPullBasic>
) : RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.event_tile_layout, parent, false)
        )
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.eventTitle.text = events[position].title
        viewHolder.eventDescription.text = when (events[position].description.length < 150) {
            true -> events[position].description
            else -> events[position].description.substring(0, 150) + "…"
        }
    }

    override fun getItemCount(): Int = events.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val eventTitle: TextView = view.findViewById(R.id.event_tile_title)
        val eventDescription: TextView = view.findViewById(R.id.event_tile_description)
    }
}
