package com.astro.front.utilities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.astro.front.Config
import com.cunoraz.tagview.Tag
import com.cunoraz.tagview.TagView
import java.sql.Timestamp
import java.text.SimpleDateFormat

class Utilities {

    companion object {
        @SuppressLint("SimpleDateFormat")
        fun formatDate(timestamp: Timestamp): String {
            return SimpleDateFormat("HH:mm, dd-MM-yyyy").format(timestamp)
        }

        fun askForLocationPermission(context: Context, activity: Activity) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                    activity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    Config.REQUEST_VALUE_LOCATION
                )
            }
        }

        fun isPermissionGranted(context: Context, permission: String) =
            ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED
    }
}

fun TagView.addTag(title: String) {
    if (tags == null) return
    val tag = Tag(title)
    tag.layoutColor = Color.GRAY
    tag.layoutColorPress = Color.DKGRAY
    addTag(tag)
}
